FROM node:18.13.0-alpine

WORKDIR /app

COPY package*.json ./

COPY prisma ./prisma/

RUN npm install

COPY . .

RUN npm install && \
    cd prisma && npm install && \
    npm install -g prisma && \
    npx prisma generate && \
    npx prisma migrate dev

EXPOSE 3000

CMD ["npm", "run", "dev" ]